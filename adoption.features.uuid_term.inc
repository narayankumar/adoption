<?php
/**
 * @file
 * adoption.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function adoption_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Dogs',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '594655ac-3bf9-49b3-820d-01bfe02c21d1',
    'vocabulary_machine_name' => 'pet_types',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Cats',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '92cd74e3-d052-4917-8a35-7aa69f5cbf7f',
    'vocabulary_machine_name' => 'pet_types',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Birds',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => 'b367d63b-717e-42c4-b283-59ba013c409a',
    'vocabulary_machine_name' => 'pet_types',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Other',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => 'b67275f7-cd25-42f0-aa71-910466571cf1',
    'vocabulary_machine_name' => 'pet_types',
    'metatags' => array(),
  );
  return $terms;
}

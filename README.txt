
build2014092102a
- deleted duplicate entries in pet types taxonomy
- added pet types exposed filter in adoption teasers view

build2014092101
- image field made compulsory
- flag's unflagging permissions only for admin, site admin and editor
- unflag text is 'adopted'
- rules message changed to 'pet adopted'
- views changes
  - image size in teasers to 16x110
  - adopted-field hide when empty (for css not to go haywire)
  - search by city exposed filter added to teasers view

build2014091801
- changed title to 'pet adoption' in teasers view
- gave it class=back, h4, second word strong
- teasers view - re-united message as red & strong
- changed link to pet-adoption
- path change to pet-adoption

build2014091602
- adoption teasers view - change , to | below image
- provide comments box
- hide sharethis label

build2014091601
- moved city above image in teasers view
- added name and date under image

7.x-1.0-dev1
- initial creation of feature on september 12 2014

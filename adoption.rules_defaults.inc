<?php
/**
 * @file
 * adoption.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function adoption_default_rules_configuration() {
  $items = array();
  $items['rules_adopted_flag_shows_adopted_message'] = entity_import('rules_config', '{ "rules_adopted_flag_shows_adopted_message" : {
      "LABEL" : "Adopted flag shows adopted message",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_adopted_flag" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "flagged-node" ], "field" : "field_adopted" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "flagged-node:field-adopted" ], "value" : "PET ADOPTED" } }
      ]
    }
  }');
  $items['rules_adopted_un_flag_shows_blank_message'] = entity_import('rules_config', '{ "rules_adopted_un_flag_shows_blank_message" : {
      "LABEL" : "Adopted un-flag shows blank message",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_unflagged_adopted_flag" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "flagged-node" ], "field" : "field_adopted" } }
      ],
      "DO" : [ { "data_set" : { "data" : [ "flagged-node:field-adopted" ] } } ]
    }
  }');
  return $items;
}

<?php
/**
 * @file
 * adoption.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function adoption_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create pet_for_adoption content'.
  $permissions['create pet_for_adoption content'] = array(
    'name' => 'create pet_for_adoption content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any pet_for_adoption content'.
  $permissions['delete any pet_for_adoption content'] = array(
    'name' => 'delete any pet_for_adoption content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own pet_for_adoption content'.
  $permissions['delete own pet_for_adoption content'] = array(
    'name' => 'delete own pet_for_adoption content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in pet_types'.
  $permissions['delete terms in pet_types'] = array(
    'name' => 'delete terms in pet_types',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any pet_for_adoption content'.
  $permissions['edit any pet_for_adoption content'] = array(
    'name' => 'edit any pet_for_adoption content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own pet_for_adoption content'.
  $permissions['edit own pet_for_adoption content'] = array(
    'name' => 'edit own pet_for_adoption content',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in pet_types'.
  $permissions['edit terms in pet_types'] = array(
    'name' => 'edit terms in pet_types',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'flag adopted_flag'.
  $permissions['flag adopted_flag'] = array(
    'name' => 'flag adopted_flag',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'publish button publish any pet_for_adoption'.
  $permissions['publish button publish any pet_for_adoption'] = array(
    'name' => 'publish button publish any pet_for_adoption',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable pet_for_adoption'.
  $permissions['publish button publish editable pet_for_adoption'] = array(
    'name' => 'publish button publish editable pet_for_adoption',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own pet_for_adoption'.
  $permissions['publish button publish own pet_for_adoption'] = array(
    'name' => 'publish button publish own pet_for_adoption',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any pet_for_adoption'.
  $permissions['publish button unpublish any pet_for_adoption'] = array(
    'name' => 'publish button unpublish any pet_for_adoption',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable pet_for_adoption'.
  $permissions['publish button unpublish editable pet_for_adoption'] = array(
    'name' => 'publish button unpublish editable pet_for_adoption',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own pet_for_adoption'.
  $permissions['publish button unpublish own pet_for_adoption'] = array(
    'name' => 'publish button unpublish own pet_for_adoption',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'unflag adopted_flag'.
  $permissions['unflag adopted_flag'] = array(
    'name' => 'unflag adopted_flag',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  return $permissions;
}

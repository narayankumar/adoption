<?php
/**
 * @file
 * adoption.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function adoption_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function adoption_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function adoption_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: ad_block_adoption
  $nodequeues['ad_block_adoption'] = array(
    'name' => 'ad_block_adoption',
    'title' => 'Ad block - Adoption',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_flag_default_flags().
 */
function adoption_flag_default_flags() {
  $flags = array();
  // Exported flag: "Adopted flag".
  $flags['adopted_flag'] = array(
    'entity_type' => 'node',
    'title' => 'Adopted flag',
    'global' => 1,
    'types' => array(
      0 => 'pet_for_adoption',
    ),
    'flag_short' => 'Set status as \'ADOPTED\'',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Cancel \'ADOPTED\' status',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => 'Adopted',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'own',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure you want to set status as \'ADOPTED\'?',
    'unflag_confirmation' => 'Are you sure you want to cancel the \'ADOPTED\' status?',
    'module' => 'adoption',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function adoption_image_default_styles() {
  $styles = array();

  // Exported image style: 340y.
  $styles['340y'] = array(
    'name' => '340y',
    'label' => '340y',
    'effects' => array(
      3 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 340,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
      12 => array(
        'label' => 'Define canvas',
        'help' => 'Define the size of the working canvas and background color, this controls the dimensions of the output image.',
        'effect callback' => 'canvasactions_definecanvas_effect',
        'dimensions callback' => 'canvasactions_definecanvas_dimensions',
        'form callback' => 'canvasactions_definecanvas_form',
        'summary theme' => 'canvasactions_definecanvas_summary',
        'module' => 'imagecache_canvasactions',
        'name' => 'canvasactions_definecanvas',
        'data' => array(
          'RGB' => array(
            'HEX' => '#ffffff',
          ),
          'under' => 1,
          'exact' => array(
            'width' => 720,
            'height' => 340,
            'xpos' => 'center',
            'ypos' => 'center',
          ),
          'relative' => array(
            'leftdiff' => '',
            'rightdiff' => '',
            'topdiff' => '',
            'bottomdiff' => '',
          ),
        ),
        'weight' => 2,
      ),
      14 => array(
        'label' => 'Crop',
        'help' => 'Cropping will remove portions of an image to make it the specified dimensions.',
        'effect callback' => 'image_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_crop_form',
        'summary theme' => 'image_crop_summary',
        'module' => 'image',
        'name' => 'image_crop',
        'data' => array(
          'width' => 720,
          'height' => 340,
          'anchor' => 'center-center',
        ),
        'weight' => 3,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function adoption_node_info() {
  $items = array(
    'pet_for_adoption' => array(
      'name' => t('Pet for adoption'),
      'base' => 'node_content',
      'description' => t('Put up a pet for adoption'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

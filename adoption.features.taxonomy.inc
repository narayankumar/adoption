<?php
/**
 * @file
 * adoption.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function adoption_taxonomy_default_vocabularies() {
  return array(
    'pet_types' => array(
      'name' => 'Pet types',
      'machine_name' => 'pet_types',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
